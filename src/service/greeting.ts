import {IGreetingServer} from '../../gRPC/hello_world_grpc_pb';
import {HelloRequest, Void, HelloResponse} from '../../gRPC/hello_world_pb';
import * as grpc from 'grpc';

export class GreetingServer implements IGreetingServer {
  sayHello(
    call: grpc.ServerUnaryCall<Void>,
    callback: grpc.sendUnaryData<HelloResponse>
  ): void {
    const response = new HelloResponse();
    response.setReply('my dear');

    callback(null, response);
  }

  talk(
    call: grpc.ServerUnaryCall<HelloRequest>,
    callback: grpc.sendUnaryData<HelloResponse>
  ): void {
    const {request} = call;

    const response = new HelloResponse();
    response.setReply(request.getMessage() + ', Reply');

    callback(null, response);
  }
}
