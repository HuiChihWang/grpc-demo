import {GreetingService} from '../gRPC/hello_world_grpc_pb';
import {GreetingServer} from './service/greeting';
import * as grpc from 'grpc';
import {ServerCredentials} from 'grpc';

const main = () => {
  const server = new grpc.Server();
  server.addService(GreetingService, new GreetingServer());

  const port = 3000;
  const address = `0.0.0.0:${port}`;

  const credentials = ServerCredentials.createInsecure();
  const portStarted = server.bind(address, credentials);

  server.start();
  console.log(`gRPC server started on port ${portStarted}`);
};

main();
