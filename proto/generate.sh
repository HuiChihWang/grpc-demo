#!/bin/bash

CURRENT_DIR=$(dirname "$0")
WORKING_DIR=$(readlink -f "$CURRENT_DIR/..")

PROTO_DIR="${WORKING_DIR}/proto"
OUTPUT_DIR="${WORKING_DIR}/gRPC"

NODE_MODULE_DIR=${WORKING_DIR}/node_modules
mkdir "$OUTPUT_DIR"

# Generate JavaScript code
grpc_tools_node_protoc \
  --proto_path="$PROTO_DIR" \
  --js_out=import_style=commonjs,binary:"$OUTPUT_DIR" \
  --grpc_out=grpc:"$OUTPUT_DIR" \
  --plugin=protoc-gen-grpc="$NODE_MODULE_DIR"/.bin/grpc_tools_node_protoc_plugin \
  "$PROTO_DIR"/*.proto

# Generate TypeScript code (d.ts)
protoc \
  --proto_path="$PROTO_DIR" \
  --plugin=protoc-gen-ts="$NODE_MODULE_DIR"/.bin/protoc-gen-ts \
  --ts_out=grpc:"$OUTPUT_DIR" \
  "$PROTO_DIR"/*.proto
